// 1.

const students = ['John', 'Joe', 'Jane', 'Jessie'];
const food = [];

function addToEnd(students, name) {
  if (typeof name == 'string') {
    students.push(name);
    return console.log(students);
  }
  return console.log('error - can only add strings to an array');
}

addToEnd(students, 'Ryan');
addToEnd(students, 045);

// 2

function addToEnd(students, name) {
  if (typeof name == 'string') {
    students.unshift(name);
    return console.log(students);
  }
  return console.log('error - can only add strings to an array');
}

addToEnd(students, 'Tess');
addToEnd(students, 033);

// 3
function elementChecker(students, name) {
  if (students.length > 1) {
    if (typeof name == 'string') {
      return console.log(students.includes(name));
    }
    return console.log('error - can only add strings to an array');
  }
  return console.log('error - can only add strings to an array');
}

elementChecker(students, 'Tess');
elementChecker([], 'Jane');
console.log('SSD');
// 4
function checkAllStringsEnding(students, char) {
  const checkArray = (students) => students.every((i) => typeof i === 'string');
  if (students.length > 1) {
    if (checkArray) {
      if (char.length === 1) {
        if (typeof char === 'string') {
          return true;
        } else {
          return console.log('error -2nd argument must be datatype string');
        }
      } else {
        console.log('error - 2nd argument must be a single character');
      }
    }
    return console.log('error - all array must be strings');
  }
  return console.log('error - array must Not be empty');
}

checkAllStringsEnding(students, 'e');
checkAllStringsEnding([], 'e');
checkAllStringsEnding(['Jane', 02], 'e');
checkAllStringsEnding(students, 'el');
checkAllStringsEnding(students, 4);
